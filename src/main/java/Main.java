import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com/");
        Random r = new Random();
        WebDriverWait wait = new WebDriverWait(driver,10);
        String randomNumber = String.format("%04d", r.nextInt(1001));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://service-devgwsams-us.secure.lab.nordigy.ru/fax-v1.0/");
        Cookie DisableCapcha = new Cookie("gwqat_disable_Captcha", "1");
        driver.manage().addCookie(DisableCapcha);
        driver.findElement(By.xpath("//*[@type=\"button\"]")).click();

        driver.findElement(By.xpath("//*[@name=\"ContactForm[firstName]\"]")).sendKeys("Something");
        driver.findElement(By.xpath("//*[@name=\"ContactForm[lastName]\"]")).sendKeys("New");
        driver.findElement(By.xpath("//*[@id=\"ContactForm_email\"]")).sendKeys("nazar.nerubayko+" + randomNumber + "@stressmailams.lab.nordigy.ru");
        driver.findElement(By.xpath("//*[@id=\"ContactForm_contactPhone\"]")).sendKeys("1650354" + randomNumber);
        driver.findElement(By.xpath("//*[@name=\"ContactForm[company]\"]")).sendKeys("Odessa-Absoft");
        driver.findElement(By.xpath("//*[@name=\"ContactForm[numberOfEmployees]\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"ContactForm_numberOfEmployees\"]/option[3]")).click();
        driver.findElement(By.xpath("//*[@name=\"ContactForm[numberOfEmployees]\"]")).click();
        driver.findElement(By.xpath("//*[@class=\"chkBox\"]")).click();
        driver.findElement(By.xpath("//*[@type=\"button\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_number\"]")).sendKeys("4111111111111111");
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_cvv\"]")).sendKeys("111");
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_type\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_type\"]/option[2]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_type\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_addressLine1\"]")).sendKeys("Elm street");
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_city\"]")).sendKeys("New York");
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_stateId\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_stateId\"]/option[23]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_stateId\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"BillingInfoForm_zipCode\"]")).sendKeys("93103");

        driver.findElement(By.xpath("//*[@class='chkBox']")).click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated
                (By.xpath("//*[@class=\"preloader\"]")));
        driver.findElement(By.xpath("//*[@class='chkBox']")).click();
        driver.findElement(By.xpath("//*[@id=\"yw5\"]/button")).click();

    }
}
